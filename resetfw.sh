#!/bin/bash
#
# resetfw.sh


# default policy: block all
iptables -P INPUT DROP

# reset all filters
iptables -F -t filter

# allow localhost by interface
iptables -A INPUT -i lo -j ACCEPT

# allow all stablished
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# reject all from 192.168.1.0/24
iptables -A INPUT -s 192.168.1.0/24 -j REJECT

# enable PING
iptables -A INPUT -s 192.168.1.0/24 -p icmp -j ACCEPT

# Accept new ssh connections
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 22 -j ACCEPT

# Reject all other packets
iptables -A INPUT -j REJECT

# vim:ft=sh:

