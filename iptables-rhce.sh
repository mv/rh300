
# restart
iptables -F

# default: block all
iptables -P INPUT DROP

# default: localhost
iptables -A INPUT -i lo -j ACCEPT

# rhce: disallow this network
iptables -A INPUT -s 192.168.1.0/24 -j DROP

# rhce: allow localnet
iptables -A INPUT -s 192.168.0.0/24 -j ACCEPT


# show
iptables -L
