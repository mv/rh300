#!/bin/bash
#
#  NAME
#       samsreport.sh
#
#  USAGE
#       samsreport.sh directory...
#
#  DESCRIPTION
#       Sam has asked you to write a shell script to generate a report
#       called "$am's Report". The report should take a list of
#       directories on the command line and will prompt for the name of
#       the report requester.
#
#       If enough arguments are not given, an error message is displayed:
#
#            usage: ./samsreport.sh directories...
#
#       For each command line argument the script should print out the
#       current argument, a colon, a space and then one of the following:
#
#           - is empty
#           - is not empty
#           - is not a directory
#
#       For example, when executing the following:
#
#           $ ./samsreport.sh Desktop Documents .bashrc
#
#       The script will prompt the user for a name:
#
#         Who is this report for?
#
#       Then, if "Jim" was entered, the script should output:
#
#         $am's Report
#         Desktop: is not empty
#         Documents: is empty
#         .bashrc: is not a directory
#         ___generated for Jim___
#
#       Sam is very particular and has asked that the output match exactly
#       as above including the dollar sign for the letter S in Sam. For
#       example, the footer should have exactly three underscores, the
#       footer message, and three more underscores without any added
#       whitespace.
#
#  CHANGELOG
#       * Tue Jan 18 2011 - George Hacker <ghacker@redhat.com>
#       - Original code

PATH=/bin:/usr/bin

# Complain and quit when no arguments are specified:
if [ $# -lt 1 ] ; then
    echo "usage: $0 directories..."
    exit 1
fi

# Prompt the user for a name:
echo -n 'Who is this report for? '
read name

# Print the header for Sam's Report.  (Note the quoting of $.)
echo "\$am's Report"

# Process each argument:
for dir in "$@" ; do

    # Confirm the current item is a directory.
    if [ -d "$dir" ] ; then
        # Empty directories have only two files: . and ..
        if [ "$(ls -a $dir | wc -l)" -eq 2 ] ; then
            echo " $dir: is empty"
        else
            echo " $dir: is not empty"
        fi
    else
        echo " $dir: is not a directory"
    fi

done

# Print the footer for Sam's Report.  (Note the variable reference.)
echo "___generated for ${name}___"
exit 0
