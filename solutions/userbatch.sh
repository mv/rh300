#!/bin/bash

# If the script was called with an argument, use it.  Otherwise, look for
# user_list.txt in the current working dir.
if [ -n "$1" ] ; then
    userfile=$1
else
    userfile=user_list.txt
fi

# Make sure the user file exists and is readable.
if [ ! -f $userfile -o ! -r $userfile ] ; then
    echo "error: unable to read $userfile"
    exit 1
fi

# Iterate over our list of users one at a time, ignoring lines that start
# with a '#' character.
for u in $(grep -v '^#' $userfile) ; do

     username="$(echo $u | cut -d: -f1)"
     password="$(echo $u | cut -d: -f2)"
     maxpwage="$(echo $u | cut -d: -f3)"
     supgroup="$(echo $u | cut -d: -f4)"

     # Create the user account.
     useradd -G $supgroup -K PASS_MAX_DAYS=$maxpwage $username

     # Set the account password.
     echo "$password" | passwd --stdin $username

done
