
# default policies
iptables -P INPUT   DROP
iptables -P FORWARD DROP
iptables -P OUTPUT  ACCEPT

# loopback
iptables -I INPUT -i lo -j ACCEPT

# ping
iptables -A INPUT -p icmp -j ACCEPT

# allow established sessions
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# ssh
iptables -A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT

iptables -A INPUT -p tcp --deport  80 -m state --state NEW -j ACCEPT
iptables -A INPUT -p tcp --deport 443 -m state --state NEW -j ACCEPT


# nat: webserver internal
iptables -t nat    -A PREROUTING -d 200.200.200.1 -j DNAT --to 172.16.0.1
iptables -t filter -A FORWARD    -d 172.16.0.1 -p tcp --dport 80 -j ACCEPT


iptables -t nat    -A PREROUTING -d 200.200.200.2 -j DNAT --to 172.16.0.2
iptables -t filter -A FORWARD    -d 172.16.0.2  -p tcp --dport 80 -j ACCEPT


# nat: intranet to UNIVERSE
iptables -t nat    -A POSTROUTING -o eth1 -j MASQUERADE
iptables -t nat    -A POSTROUTING -o eth1 -j MASQUERADE


iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE


iptable -t filter -A INPUT -o lo -j ACCEPT

iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROPT
iptables -t filter -P OUTPUT ACCEPT

iptables -t filter -A INPUT -m state --state NEW -t tcp --dport 22 -j ACCEPT
iptables -t filter -A INPUT -m state --state NEW -t tcp --dport 80 -j ACCEPT


# nat outbound
iptables -t nat -A POSTROUTING -i eth1 -j MASQUERADE


# nat inbound: 200.200.200.1:80 -> 172.16.0.1:80
iptables -t nat    -A PREROUTING -d 200.200.200.1 -j DNAT --to 172.16.0.1
iptables -t filter -A FORWARD  -p tcp --dport 80 -j ACCEPT

iptables -t nat    -A PREROUTING -d 200.200.200.1 -j DNAT --to 172.16.0.1
iptables -t filter -A FORWARD -p tcp --dport 80 -j ACCEPT



iscsiadm -m discovery -p 192.168.0.254 -t st
iscsiadm -m node   -l -p 192.168.0.254 -T iqn.2010.com.example:rdisks.server17

iscsiadm -m discovery -p 192.168.0.254 -t st
iscsiadm -m node   -l -p 192.168.0.254 -T iqn.2010.com.example


cryptsetup luksFormat /dev/sda1
cryptsetup luksOpen   /dev/sda1 mysecret # /dev/mapper/mysecret
cryptsetup luksAddkey /dev/sda1 /root/.access

#/etc/crypttab
mysecret  /dev/sda1  /root/.access

#/etc/fstab
/dev/mapper/mysecret   /data   ext4  defaults 1 2




cryptsetup luksFormat /dev/sda1
cryptsetup luksOpen   /dev/sda1 mysecret  # /dev/mapper/mysecret
cryptsetup luksAddKey /dev/sda1 /root/.access


#/etc/crypttab
mysecret  /dev/sda1   /root/.access


semanage fcontext -a -f "" -t httpd_sys_content_t '/var/www(/.*)?'
restorecon -FRvv /var/www


semanage user -l |
semanage port -l |
semanage fcontext -l |

getsebool -a |
setsebool -P samba_export_all_ro 1

sealert -l UUID
