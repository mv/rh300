#!/bin/bash
#
# iptables rules for a public server
#
# Marcus Vinicius Ferreira     ferreira.mv[ at ]gmail.com
#
# 2013-07
#

###
### iptables [-t table] -command chain rule-specification target [options]
###
### Obs:
###   table  : filter,nat,mangle,raw
###   chain  : INPUT,OUTPUT,FORWARD,PREROUTING,POSTROUTING
###   command: -F -L -P -A -I -D -R -E -Z -N -X
###   target : ACCEPT,DROP,REJECT,QUEUE,RETURN, and
###            DNAT,SNAT,MASQUERADE,MIRROR,REDIRECT,LOG,TARPIT
###   options: -v -n -x --line-numbers
###

### Begin again
iptables -F

###
### Default Policies
###   policies only use ACCEPT/DROP. Do not use REJECT.
###
###   iptables [-t table] -P chain target
###

iptables -t filter -P INPUT  DROP
iptables -t filter -P FORWAD DROP
iptables -t filter -P OUTPUT ACCEPT

###
### Rules
###   iptables [-t table] -I chain [num] rule-specification
###   iptables [-t table] -A chain       rule-specification
###   iptables [-t table] -D chain       rule-specification
###
###   iptables  -t filter -A INPUT       rule-specification [REJECT [--type x]]
###   iptables  -t filter -A FORWARD     rule-specification [REJECT [--type x]]
###   iptables  -t filter -A OUTPUT      rule-specification [REJECT [--type x]]
###   iptables  -t nat    -A PREROUTING  [-DNAT|REDIRECT]
###   iptables  -t nat    -A OUPUT       [-DNAT|REDIRECT]
###   iptables  -t nat    -A POSTROUTING [-SNAT|MASQUERADE [--to-ports port[-port]]]
###
###   iptables  -t mangle -A INPUT|OUTPUT|FORWAD|PREROUTING|POSTROUTING
###   iptables  -t raw    -A OUPUT|PREROUTING
###

### rule-specification
###   -p tcp  --sport xx[:yy] -s source.ip
###   -p tcp  --dport xx[:yy] -d source.ip
###   -p upd  --sport xx[:yy] -s source.ip
###   -p upd  --dport xx[:yy] -d source.ip
###   -p sctp --dport xx[:yy]
###   -p icmp --icmp-type echo-reply
###   -p icmp --icmp-type destination-unreachable
###
###   -i input-interface
###   -o output-interface
###
###   -m matchname
###
###   -m comment   --comment "256 chars here"
###   -m connbytes --connbytes-xxx yyy
###   -m connlimit --connlimit-above xx
###   -m connlimit --connlimit-mask  xx
###   -m state     --state NEW
###   -m state     --state ESTABLISHED,RELATED
###   -m state     --state INVALID
###   -m tcp       --sport port[:port]
###   -m tcp       --dport port[:port]
###   -m tcp       --tcpflags SYN,RST,ACK,FIN SYS
###   -m tcp       --syn
###   -m ttl       --ttl-eq xx
###   -m ttl       --ttl-gt xx
###   -m ttl       --ttl-lt xx
###   -m time      --datestart YYYY[-MM[-DD[Thh[:mm[:ss]]]]] [--utc|--localtz]
###   -m time      --datestop  YYYY[-MM[-DD[Thh[:mm[:ss]]]]] [--utc|--localtz]
###   -m time      --timestart hh:mm[:ss]                    [--utc|--localtz]
###   -m time      --timestop  hh:mm[:ss]                    [--utc|--localtz]
###   -m time      --monthdays aa[,bb,cc]
###   -m time      --weekdays  aa[,bb,cc]
###
###   ? iprange: --src-range | --dst-range
###   ? mac: --mac-source
###     limit: --limit 10/second 50/minute 10/hour 1000/day
###     limit: --limit-burst 5
###
###   ? owner uid !!!
###   ? quota !!!
###   ? LOG !!!
###   ? MIRROR !!!
###   ? -t nat SAME !!!
###
###   -j target
###




### options
###   -v
###   --line-numbers
###
iptables -L v --line-numbers


# vim:ft=sh:

