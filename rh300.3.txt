
### SELinux

'Application firewall'
- based on contexts
  - "resources"
     - httpd_t
     - httpd_sys_content_t
     - ...
     - root_u
     - system_u - user
     - object_r - file
     - xxxxxx_x - process


# list contexts defineds on disk
ls -Z /
ls -Z /var | sort -k4 | column -t


# list all se definitions
semanage

semanage fcontext -l
semanage fcontext -l | grep http

semanage port -l

# change your context
getenforce

setenforce 0 # Permissive
setenforce 1 # Enforcing

chcon

# set corp just like /var/www
chcon --reference /var/www /corp

        # make it permanent
semanage fcontext -a -f "" -t httpd_sys_context '/corp(/.*)?' 
restorecon -FRvv /corp


chattr

restorecon 
restorecon -FRvv /var/www

# disable/enable, must reboot
    # on /etc/selinux/config
    SELINUX=disabled

walber.ss@gmail.com
nullck@yahoo.com.br

# Booleans
setsebool -a | grep http | grep 'user|home'
setsebool httpd_enable_homedirs on   # enable apache 'UserDir' functionality

## Tip: system-config-selinux

## Tip: relabel
   touch /.autorelabel
   reboot

## Tip: setroubleshoot.rpm


# firewall

 Type Chains:
  - filter
    - INPUT
    - OUTPUT
    - FORWARD
  - nat
  - mangle
  - raw

  States
     - NEW
     - ESTABLISHED: connection already done (rule from same s/d/port)
     - INVALID: 
        - a connection without handshake
        - a connection without a previous required state
     - RELATED
        - ftp scenario: begin in one port, works in another

iptables -A   # add rule to the end of the chain
iptables -I   # add rule
iptables -I 5 # add rule to position 5

iptables -F   # Flush all rules

iptables -A|-I [chain] -s       x.x.x.x -j ACCEPT
iptables -A|-I [chain] --source x.x.x.x -j ACCEPT

iptables -A|-I [chain] -d x.x.x.x  -p tcp|udp -dport y -m state --state NEW -j ACCEPT
iptables -A|-I [chain] -m state --state STABLISHED -j ACCEPT


iptables -A|-I [chain] -s x.x.x.x  -p tcp|udp -dport y -i eth0 -j ACCEPT


# NAT

pre-req: net.ipv4.ip.forwarding = 1

State table:
  - source/sport  |  destination/dport

# OUTBOUND
# using masquerade
iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE

# using snat
iptables -t nat -A POSTROUTING -j SNAT --to 200.x.a.b


# INBOUND

# www1: 200.200.200.201:80 -> 172.168.0.1:80
iptables -t nat -A PREROUTING -d 200.200.200.201 \
                    -j DNAT --to 172.16.0.1
iptables -t filter -A FORWARD -d 172.16.0.1 -p tcp --dport 80 -j ACCEPT 

# TIPTIPTIP: one rule for nat ONLY
#          : another rule filtering. This saves CPU

# dns1: 200.200.200.202:53 -> 172.168.0.2:53
iptables -t nat -A PREROUTING -d 200.200.200.202 \
                    -j DNAT --to 172.16.0.2
iptables -t filter -A FORWARD -d 172.16.0.2 -p tcp --dport 53 -j ACCEPT 
iptables -t filter -A FORWARD -d 172.16.0.2 -p udp --dport 53 -j ACCEPT 




### syslog


# udp
*.user          @192.168.0.254

# tcp
*.user          @@192.168.0.254


### Apache

    NameVirtualHost *:80

    <VirtualHost *:80>
       ServerName site1
    </virtualHost>

    <VirtualHost *:80>
       ServerName site2
    </virtualHost>

### Apache auth

    AuthName "Realm Name"
    AuthType Basic
    AuthUserFile /etc/httpd/.htpasswd
    Require valid-user

    AuthName "Realm Name"
    AuthType Basic
    AuthBasicProvider ldap
    AuthLDAPurl "ldap://fqdn/prefis" TLS
    Require valid-user

    # Global:
    LDAPTrustedGlobalCert CA_BASE64 /etc/httpd/ca-example.crt



###

    Vhost: server17

       DocumentRoot /var/www/html


    Vhost: www17

       DocumentRoot /www17/html
       Directory    /www17/html/private
       ScriptAlias  /cgi-bin/   /var/www/cgi-bin/


