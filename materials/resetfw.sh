#!/bin/bash
# Set INPUT chain default policy to DROP
iptables -P INPUT DROP

# Flushes all rule in the filter table
iptables -F

# Will ACCEPT all packets from loopback interface
iptables -A INPUT -i lo -j ACCEPT

# ACCEPT all ESTABLISHED, RELATED packets
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# ACCEPT all NEW connections to tcp port 22
iptables -A INPUT -m state --state NEW -p tcp --dport 22 -j ACCEPT

# REJECT all packets from 192.168.1.0/24 network
iptables -A INPUT -s 192.168.1.0/24 -j REJECT

# ACCEPT all icmp traffic from 192.168.0.0/24
iptables -A INPUT -p icmp -s 192.168.0.0/24 -j ACCEPT

# REJECT all other traffic
iptables -A INPUT -j REJECT

