#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define	ELEPHANT_SZ	50*1024*1024
#define	PROCESS101_SZ	50*1024


int main (int argc, char *argv[])
{
	int pid;
	char *p, *buffer;

	/* First child = CPU hog. */
	if ((pid = fork()) == 0) {
		while ( 1 )
			pid += 9;
	}
}
