#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define	ELEPHANT_SZ	128*1024*1024
#define	PROCESS101_SZ	50*1024


int main (int argc, char *argv[])
{
	int pid;
	char *p, *buffer;

	/* Second child = memory hog. */
	if ((pid = fork()) == 0) {
		buffer = malloc(ELEPHANT_SZ);
		if (buffer != NULL) {
			for (p = buffer; p < buffer + ELEPHANT_SZ; p += 4096)
				*p = 'E';
		}
		sleep(4*3600);
	}
}
