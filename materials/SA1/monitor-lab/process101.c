#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/prctl.h>

#define	ELEPHANT_SZ	50*1024*1024
#define	PROCESS101_SZ	50*1024


int main (int argc, char *argv[])
{
	int pid;
	char *p, *buffer;

	/* Third child = process101. */
	if ((pid = fork()) == 0) {
		buffer = malloc(PROCESS101_SZ);
		if (buffer != NULL) {
			for (p = buffer; p < buffer + PROCESS101_SZ; p += 4096)
				*p = 'E';
		}
		while ( 1 ) {
			pid = 0;
			while (pid < 100000000)
				pid++;
			usleep(500000);
		}
	}

}
