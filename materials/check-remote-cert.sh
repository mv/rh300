#!/bin/bash

# usage: check-remote-cert.sh HOST:PORT

SERVER=$1

echo | # echo is needed to make server hang up
openssl s_client -connect ${SERVER} 2>/dev/null | # s_client connects and prints cert
openssl x509 -noout -subject -issuer -dates # x509 reads the cert and prints info
