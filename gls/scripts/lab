#!/bin/bash
# THIS CAN BE REPLACED BY lab-setup-server
#
# by: Brian Butler (20100907)
#
# Setup script for the Taylor/Locke lab
# this script is to be installed and run on desktopX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT

DEBUG=false
ERROR_MESSAGE="Error running script. Contact your instructor if you continue to see this message."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"

set -o pipefail

# Perform basic sanity checks on command-line arguments
if [ "$#" -eq "1" ]; then
    LAB=$1
    LABCOUNT=$(elinks --dump "http://instructor.example.com/cgi-bin/breakme.cgi?lab=${LAB}&query=True")
    echo "Lab ${LAB} has the following sequences: ${LABCOUNT}"
    echo "To setup a lab run: $(basename $0) <LABNO> seq <SEQUENCENO>"
    exit 0
fi

if [ "$#" -ne "3" ]; then
    ERROR_MESSAGE="Usage: $(basename $0) <LABNO> seq <SEQUENCENO>"
    exit 1
fi

LAB=$1
SEQUENCE=$3

# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "desktop"

RESULT=$(elinks --dump "http://instructor.example.com/cgi-bin/breakme.cgi?lab=$LAB&sequence=$SEQUENCE")
if [ "${RESULT}" != "   OK" ]; then
    ERROR_MESSAGE="Error setting up lab ${LAB} sequence ${SEQUENCE}:    $RESULT"
    exit 1
fi

reset_VT

# Reset vserver
(/usr/local/sbin/lab-resetvm 2>&1 | log) || exit

echo "Virtual machine reset sucessful, for hints and instructions please visit:"
echo "http://instructor.example.com/cgi-bin/hints.cgi"
echo "CTRL+clicking the above link will open it in firefox"
echo "done!"

exit 0
# vim: ts=4 sts=4 sw=4 ai et
