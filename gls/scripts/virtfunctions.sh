#!/bin/bash
# virt-functions.sh: rh242 vm library
# Wander Boessenkool <wboessen@redhat.com> 

INSTALLTREE="ftp://instructor.example.com/pub"
VGNAME=vol0
VMMEM=256
LASTIPOCTET=$(hostname -i | cut -d. -f4)
VSERVERMAC="00:16:3e:ca:fe:$(printf '%02x' ${LASTIPOCTET})"
VCLIENTMAC="00:16:3e:be:ef:$(printf '%02x' ${LASTIPOCTET})"

die() {
    echo $1
    exit 132
}

is_running() {
    [ -n "$(virsh list | grep $1)" ] && return 0 || return 1
}


clone () {
    lab-resetvm
}


# vim: ai ts=4 sts=4 sw=4 et
