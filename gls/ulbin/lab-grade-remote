#!/bin/bash
#set -x

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=remoteGrade
DEBUG=false
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail

# Check to make sure we are running on the correct host
check_host "server"

# Check to make sure we are running as root
check_root

# Get X
get_X

# Variables
NAME=`basename ${0}`
SSH_OPTS='-o PreferredAuthentications=publickey -o StrictHostKeyChecking=no'

function FAIL () {
        echo "$*"
        print_FAIL
        echo Grading did not pass.  Please try again.
        exit 1
}

function GOOD () {
        print_PASS
        echo "Good, $*"
}


# Check to see if we can log in to desktopX w/ a key pair
function ssh_key_one_way {
    su -c "ssh $SSH_OPTS student@desktop${X} sleep 1" student
}

# Check the contents of the /tmp/student.tar.bz2 file
function verify_tarball {
    local TARBALL=/tmp/student.tar.bz2
    local TESTDIR=${NAME}.$$
    local RESTORED=
    if [ -r "${TARBALL}" ] ; then
	mkdir -p /tmp/${TESTDIR}/tarball
	su -c "ssh $SSH_OPTS desktop${X} 'find . -type f -exec sha256sum {} \;'" student > /tmp/${TESTDIR}/SHA256SUM
	pushd /tmp/${TESTDIR}/tarball &>/dev/null
	
	su -c "tar xf ${TARBALL} &>/dev/null" student
	popd &>/dev/null

    else
	echo "${TARBALL} does not exist."
        return 1
    fi
}

# Check the copy of /tmp/student.tar.bz2 on desktopX
function verify_copy {
	SRC=/tmp/student.tar.bz2
	DST=/tmp/a.$$.tar.bz2
	su -c "scp $SSH_OPTS desktop${X}:$SRC $DST" student || return 
	diff -q $SRC $DST > /dev/null && rm $DST
}

ssh_key_one_way || FAIL "could not ssh."
verify_tarball || FAIL "could not verify tarball."
verify_copy || FAIL "could not verify copy."

GOOD script succeeded.

# Clean up
rm -rf /tmp/a.$$.tar.bz2 
rm -rf /tmp/${NAME}.$$
