#!/bin/bash
#
# by: Robert Locke (20101130)
# based on: Brian Butler
#
# Grade the first packages assessment
# this script is to be installed and run on serverX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=centralstoreGrade
DEBUG=false
ERROR_MESSAGE="FAILED."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail
RPM=/bin/rpm

# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "server"

# Check for new packages
PACKAGES="xsane gimp"
echo
echo -n "Checking for ${PACKAGES}..."
success=1
for package in ${PACKAGES} ; do
  $RPM -q ${package} &>/dev/null || success=0
done
[ ${success} -eq 1 ] && print_PASS || (print_FAIL; exit 1)

# Check for updated package
PACKAGES="dmidecode"
echo
echo -n "Checking for updates to ${PACKAGES}..."
success=1
for package in ${PACKAGES} ; do
  $RPM -q ${package} | grep "2\.11-2\.el6_1" &>/dev/null || success=0
done
[ ${success} -eq 1 ] && print_PASS || (print_FAIL; exit 1)

PACKAGES="libtiff"
echo
echo -n "Checking for updates to ${PACKAGES}..."
success=1
for package in ${PACKAGES} ; do
  $RPM -q ${package} | grep "3\.9\.4-6\.el6_3" &>/dev/null || success=0
done
[ ${success} -eq 1 ] && print_PASS || (print_FAIL; exit 1)

# Check for removed package
PACKAGES="vsftpd"
echo
echo -n "Checking for removal of ${PACKAGES}..."
success=1
for package in ${PACKAGES} ; do
  $RPM -q ${package} &>/dev/null && success=0
done
[ ${success} -eq 1 ] && print_PASS || (print_FAIL; exit 1)

exit 0
