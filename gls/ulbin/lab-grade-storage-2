#!/bin/bash
#
# by: Robert Locke (20101130)
# based on: Brian Butler
#
# Grade the second storage lab
# this script is to be installed and run on serverX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=lvmGrade
DEBUG=false
ERROR_MESSAGE="FAILED."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail

# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "server"

# Ask if we have just rebooted
echo
echo "You need to reboot right before running this grading script."
echo
echo "Press ENTER if you have just rebooted, otherwise press"
echo "Ctrl-C to abort grading, reboot, then run this script again."
read PAUSEMETOREAD

# Test for additional swap space
origswap="/dev/dm-0"
echo
echo "Checking for multiple swap spaces... "
[ $(tail -n +2 /proc/swaps | wc -l) -eq 2 ] && print_PASS || (print_FAIL; exit 1)
echo
echo "Checking for size of second swap space... "
swapsize=$(tail -n +2 /proc/swaps | grep -v "${origswap}" | cut -f2)
[ ${swapsize} -gt 475000 -a ${swapsize} -lt 530000 ] && print_PASS || (print_FAIL; exit 1)

# Test that filesystem is mounted
fsmount="/opt"
echo
echo "Checking for mounted filesystem ${fsmount}... "
df -h ${fsmount} &>/dev/null && print_PASS || (print_FAIL; exit 1)
# Test attributes of mounted filesystem
echo
echo "Checking for filesystem type of ${fsmount}... "
df -T ${fsmount} | grep ext4 &>/dev/null && print_PASS || (print_FAIL; exit 1)
echo
echo "Checking size of filesystem at ${fsmount}... "
fssize=$(df -T ${fsmount} | tr -s ' ' | tail -n 1 | cut -d' ' -f3)
[ ${fssize} -gt 475000 -a ${fssize} -lt 530000 ] && print_PASS || (print_FAIL; exit 1)
echo
echo "Checking for acl support on ${fsmount}... "
fspart=$(grep "${fsmount} " /etc/mtab | cut -d' ' -f1)
if tune2fs -l ${fspart} | grep "Default mount.*acl" &>/dev/null
then
  print_PASS
elif mount | grep "${fsmount} .*acl" &>/dev/null
then
  print_PASS
else
  (print_FAIL; exit 1)
fi

exit 0
