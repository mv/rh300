#!/bin/bash
#
#  Modify vserver's network configuration.

#Add labtool functions
. /usr/local/lib/labtool.shlib
trap on_exit EXIT

# Get X
get_X

PATH=/bin:/usr/bin:/sbin:/usr/sbin
domain=vserver
network="none"

if [ $# != 0 ]; then
    case "$1" in
        '-p' | '--private')
            network='private'
            [ ${#2} != 0 ] && domain=$2
            ;;
        '-c' | '--classroom')
            network='classroom'
            [ ${#2} != 0 ] && domain=$2
            ;;
        'vserver')
            ;;
        'NAME OF SERVER200+X')
            domain=$1
            ;;
        *)
            echo 'Reconfiguration aborted.'
            echo
            echo 'usage: gls-vserver-network2 [OPTION] [DOMAIN]'
            echo
            echo '  -p, --private       vserver is configured for private network'
            echo '  -c, --classroom     vserver is configured for classroom network'
            echo
            echo 'If no DOMAIN is specified then vserver is the default.'
            echo
            echo 'If no option is passed enter interactive mode.'
            exit 1
            ;;
    esac
fi

if [ "${network}" = "none" ] ; then
    echo "Reconfigure ${domain}'s Networking"
    echo
    echo '  p = private network'
    echo '  c = classroom network'		# bridged interface
    echo
    echo -n "Which network should ${domain} be connected to [c/p]? "
    read resp
    case "${resp}" in
       "c" | "C")
    	network=classroom
	    ;;
        "p" | "P")
	    network=private
	    ;;
        *)
	    echo 'Reconfiguration aborted.'
	    exit 1
	    ;;
    esac

    echo '*** WARNING ***'
    echo -n "Are you sure you want to reset ${domain} [y/N]? "
    read resp
    case "${resp}" in
        "y" | "Y" | "yes" | "YES")
    	;;
        *)
	    echo 'Reset aborted.'
	    exit 1
	    ;;
    esac
fi

#Add check for VT, if on VT abort on any network other than classroom
check_VT
if [ "${VT}" = "true" ]; then
    if [ "${network}" != "classroom" ]; then
        log "Attempting to change network on a VT machine"
        ERROR_MESSAGE="Cannot change network in VT environment!"
        exit 1
    else
        exit 0
    fi
fi


if ! virsh dominfo "${domain}" >& /dev/null
then
	echo "Error: ${domain} is not a valid domain name."
	exit 1
fi

# Shut down the virtual machine.
echo "Shuting down ${domain} virtual machine."
virsh destroy "${domain}" >& /dev/null

# Configure the virtual machine to use the desired network
echo "Tuning ${domain} to use the ${network} network."
virsh dumpxml ${domain} > /tmp/domain-$$.xml
if [[ "${network}" = classroom ]]
then
	iface_orig="<interface type='network'>"
	iface_new="<interface type='bridge'>"
	source_orig="<source network='default'/>"
	source_new="<source bridge='br0'/>"

	cat > /tmp/network-$$.xml << EOF
<network>
  <name>default</name>
  <uuid>$(virsh net-uuid default)</uuid>
  <forward mode='nat'/>
  <bridge name='virbr0' stp='on' delay='0' />
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254' />
    </dhcp>
  </ip>
</network>
EOF

elif [[ "${network}" = private ]]
then
	iface_orig="<interface type='bridge'>"
	iface_new="<interface type='network'>"
	source_orig="<source bridge='br0'/>"
	source_new="<source network='default'/>"

	cat > /tmp/network-$$.xml << EOF
<network>
  <name>default</name>
  <uuid>$(virsh net-uuid default)</uuid>
  <bridge name='virbr0' stp='on' delay='0' />
  <ip address='10.${X}.0.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='10.${X}.0.2' end='10.${X}.0.254' />
    </dhcp>
  </ip>
</network>
EOF

fi
sed -i -e "s:${iface_orig}:${iface_new}:" \
       -e "s:${source_orig}:${source_new}:" /tmp/domain-$$.xml
virsh define /tmp/domain-$$.xml
virsh net-destroy default
virsh net-define /tmp/network-$$.xml
virsh net-start default
# rm -f /tmp/domain-$$.xml /tmp/network-$$.xml

echo "Starting new instance of ${domain} virtual machine."
virsh start "${domain}" >& /dev/null

exit 0
