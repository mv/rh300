#!/bin/bash
#
# by: Robert Locke (20101130)
# based on: Brian Butler
#
# Grade the third file management lab
# this script is to be installed and run on serverX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=fm3Grade
DEBUG=false
ERROR_MESSAGE="FAILED."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail

# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "server"

# Check for files distributed to proper places.
echo
echo "Checking for files to be moved to photos subdirs... "
echo "  Checking for family photos"
find /home/brad -name 'jenny*.jpg' | grep -v photos/family &>/dev/null && print_FAIL || print_PASS

echo "  Checking for new_york photos"
( find /home/brad -name 'new_york*.jpg' | grep -v photos/places &>/dev/null ) && print_FAIL || print_PASS

echo "  Checking for tokyo photos"
( find /home/brad -name 'tokyo*.jpg' | grep -v photos/places &>/dev/null ) && print_FAIL || print_PASS

echo "  Checking for work photos"
( find /home/brad -name 'redhat*.jpg' | grep -v photos/work &>/dev/null ) && print_FAIL || print_PASS

# Check that all files with bad in the name are gone.
echo
echo "Checking that all bad files are gone... "
list=$(find /home/brad -name '*bad*.jpg' 2>/dev/null)
[ -z "${list}" ] && print_PASS || print_FAIL

# Check for symbolic link
echo
echo "Checking for symbolic link... "
if [ -L /home/brad/photos/jenny ] ; then
# Work with absolute or relative path (from photos/)
( ls -l /home/brad/photos/jenny | egrep '/home/brad/photos/jenny -> (/home/brad/photos/)?family' &>/dev/null ) && print_PASS || print_FAIL
else
	print_FAIL
	echo "/home/brad/photos/jenny is not a symbolic link."
fi


exit 0
