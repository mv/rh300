#!/bin/bash
#
# by: Brian Butler (20100902)
#
# Setup the Morris Worm and Fish Supply Company Case Study
# this script is to be installed and run on desktopX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=morrisWorm
DEBUG=false
ERROR_MESSAGE="Error running script. Contact your instructor if you continue to see this message."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail

# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "desktop"

# Prompt the user before we make any changes to the system
cat <<EOL

*** NOTICE ***
This script will reset vserver to setup the excercise.
EOL
confirm
echo
echo -n "Setting up, one moment please... "

# Reset vserver
(/usr/local/sbin/lab-resetvm 2>&1 | log) || exit

#Work in VT as well
reset_VT

# Back up existing iptables rules
([ -f /etc/sysconfig/iptables ] && cp -a /etc/sysconfig/iptables /root/iptables 2>&1 | log)

# Clear iptables rules
(service iptables stop 2>&1 | log) || exit


check_VT
if [ "${VT}" = "true" ]; then

  #Add an alias to desktopX for the virtual network:
  get_X
  cp /etc/sysconfig/network-scripts/ifcfg-eth0 /tmp/ifcfg-eth0.premorris
  cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DEVICE=eth0
NM_CONTROLLED="yes"
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.0.${X}
PREFIX=24
GATEWAY=192.168.0.254
DNS1=192.168.0.254
IPADDR2=10.42.${X}.254
PREFIX2=24
EOF

  #Wait for serverX to re-appear on the network
  wait_tcp_port server${X}.example.com 22
  push_sshkey server${X}.example.com &> /dev/null
  sshKey="/root/.ssh/.labtoolkey"
  cat > /tmp/serverX-network <<EOF
DEVICE="eth0"
BOOTPROTO=static
NM_CONTROLLED="yes"
IPADDR=10.42.${X}.100
PREFIX=24
GATEWAY=10.42.${X}.254
DNS1=192.168.0.254
ONBOOT=yes
EOF
  scp -i ${sshKey} /tmp/serverX-network root@server${X}.example.com:/etc/sysconfig/network-scripts/ifcfg-eth0 2>&1 | log
  #Restart our networking
  ssh -f -i ${sshKey} root@server${X}.example.com "nmcli con up id 'System eth0'"
  nmcli con up id 'System eth0' 2>&1 | log
else
  # Configure vserver to use the private network
  (gls-vserver-network --private 2>&1 | log) || exit
fi

echo 'done!'

exit 0
