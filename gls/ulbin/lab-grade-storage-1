#!/bin/bash
#
# by: Robert Locke (20101130)
# based on: Brian Butler
#
# Grade the first storage lab
# this script is to be installed and run on serverX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=lvmGrade
DEBUG=false
ERROR_MESSAGE="FAILED."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail

# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "server"

# Ask if we have just rebooted
echo
echo "You need to reboot right before running this grading script."
echo
echo "Press ENTER if you have just rebooted, otherwise press"
echo "Ctrl-C to abort grading, reboot, then run this script again."
read PAUSEMETOREAD

# Test that filesystem is mounted
fsmount="/extras"
echo
echo "Checking for mounted filesystem ${fsmount}... "
df -h ${fsmount} &>/dev/null && print_PASS || (print_FAIL; exit 1)

# Test attributes of mounted filesystem
echo
echo "Checking for non-encryption of ${fsmount}... "
fspart=$(grep "${fsmount} " /etc/mtab | cut -d' ' -f1)
cryptsetup status ${fspart} &>/dev/null && (print_FAIL; exit 1) || print_PASS
echo
echo "Checking for filesystem type of ${fsmount}... "
df -T ${fsmount} | grep ext4 &>/dev/null && print_PASS || (print_FAIL; exit 1)
echo
echo "Checking size of filesystem at ${fsmount}... "
fssize=$(df -T ${fsmount} | tr -s ' ' | tail -n 1 | cut -d' ' -f3)
[ ${fssize} -gt 950000 -a ${fssize} -lt 1100000 ] && print_PASS || (print_FAIL; exit 1)

exit 0
