#!/bin/bash
#
# by: Robert Locke (20101130)
# based on: Brian Butler
#
# Grade the LVM lab
# this script is to be installed and run on serverX.example.com
# output is logged to syslog

# Set environment and declare global variables
. /usr/local/lib/labtool.shlib
trap on_exit EXIT
LOG_FACILITY=local0
LOG_PRIORITY=info
LOG_TAG=lvmGrade
DEBUG=false
ERROR_MESSAGE="FAILED."
PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin
userInput="no"
set -o pipefail
newvg=extra
newlv=iso
newlv_mount=/$newlv
HOME="vgsrv/home"


# Check to make sure we are running as root
check_root

# Check to make sure we are running on the correct host
check_host "server"

# Ask if we have just rebooted
echo
echo "You need to reboot right before running this grading script."
echo
echo "Press ENTER if you have just rebooted, otherwise press"
echo "Ctrl-C to abort grading, reboot, then run this script again."
read PAUSEMETOREAD

# Check size of $HOME LV
echo
echo "Checking size of $HOME LV"
SIZE=`lvs --noheadings -o lv_size $HOME | awk -F . '{print $1}'`
if [[ $SIZE -lt 400 ]];then
	print_FAIL; echo "$HOME LV is too small"
elif [[ $SIZE -gt 600 ]];then
	print_FAIL; echo "$HOME LV is too large"
else
	print_PASS
fi

# Check size of $HOME FS
echo
echo "Checking size of $HOME filesystem"
SIZE=`df -m /home | grep /home | awk '{print $1}'`
if [[ $SIZE -lt 400 ]];then
	print_FAIL; echo "$HOME filesystem is too small"
elif [[ $SIZE -gt 600 ]];then
	print_FAIL; echo "$HOME filesystem is too large"
else
	print_PASS
fi

# Check $newvg exists
echo
echo "Checking that $newvg exists"
if vgs $newvg |grep $newvg &> /dev/null;then
	print_PASS
else
	print_FAIL; echo "$newvg does not exist"
fi

# Check $newvg/$newlv exists
echo
echo "Checking that $newvg/$newlv exists"
if lvs $newvg/$newlv &> /dev/null;then
	print_PASS
else
	print_FAIL; echo "$newvg/$newlv does not exist"
fi

# Check that $newlv_mount is mounted
echo
echo "Checking that $newlv_mount is mounted"
if df | grep -q $newlv_mount ; then
	print_PASS
else
	print_FAIL; echo "$newlv_mount is not mounted"
fi

# Check that $newlv_mount is mounted persistently
echo
echo "Checking that $newlv_mount is mounted persistently"
if grep -q $newlv_mount /etc/fstab ; then
	print_PASS
else
	print_FAIL; echo "$newlv_mount will not be mounted at boot time"
fi

# Check that $newlv_mount is ext4
echo
echo "Checking that $newlv_mount is ext4"
if df -T | grep $newlv_mount | grep -q ext4; then
	print_PASS
else
	print_FAIL; echo "$newlv_mount is not formated with ext4"
fi

# Check that $newvg is using multipath
echo
echo "Checking that $newvg is using multipath"
set +o pipefail
if pvs | grep -q "mpath.*$newvg" ; then
	print_PASS
else
	print_FAIL; echo "$newvg is not using multipath"
fi

exit 0
