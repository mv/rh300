#!/bin/bash

. /usr/local/lib/labtool.shlib || echo 'Error: labtool.shlib not found!'
trap on_exit EXIT

# "elephant" should not be running
ELEPHANT=bloatware

# "hippo" should be niced to the value of HIPPO_NICE
HIPPO=running-circles
HIPPO_NICE=10

DU_FILE=/home/student/Desktop/usr-directory.txt
DF_FILE=/home/student/Desktop/least-free-space-filesystem.txt

# make sure we are running on server
check_host "server"


echo -n "* Checking for terminated process... "

if ps -C "$ELEPHANT" &>/dev/null
then
    print_FAIL
    echo
    echo "The \"memory hogging\" process is still running."
    echo
else
    print_PASS
fi


echo -n "* Checking for reniced process... "

HN="$(ps -C "$HIPPO" -o nice= | tr -d '[:space:]')"

if [[ $HN == $HIPPO_NICE ]]
then
    print_PASS
else
    print_FAIL
    echo
    echo "The \"CPU hogging\" process does not have a priority of ${HIPPO_NICE}."
    echo
fi



echo -n "* Checking for /usr directory... "

WCDU="$(wc -l $DU_FILE | awk '{print $1}')"

if [[ $WCDU == 1 ]]
then
    print_PASS
    echo -n "* Checking for /usr match... "

    USR_DIR=$(du -sm /usr/* | sort -n | awk '{print $2}' | tail -1)

    if grep -q $USR_DIR $DU_FILE
    then
        print_PASS
    else
        print_FAIL
        echo
        echo "That is the incorrect directory.  Try again."
        echo
    fi

else
    print_FAIL
    echo
    echo "The $DU_FILE file has more than one line.  Remove all lines except the line that matches the criterion."
    echo
fi
echo -n "* Checking for file systems... "

WCDF="$(wc -l $DF_FILE | awk '{print $1}')"

if [[ $WCDF == 1 ]]
then
    print_PASS
    echo -n "* Checking for file system match... "

    FS=$(df | cut -c 41-80 | grep / | sort -rn | awk '{print $3}' | tail -1)
    
    if grep -q $FS $DF_FILE
    then
        print_PASS
    else
        print_FAIL
        echo
        echo "That is the incorrect file system.  Try again."
        echo
    fi

else
    print_FAIL
    echo
    echo "The $DF_FILE file has more than one line.  Remove all lines except the line that matches the criterion."
    echo
fi



exit
    
