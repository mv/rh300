
mkdir -p /opt/research

chown root:grads  /opt/research
chmod 0770        /opt/research
chmod g+s         /opt/research   # ownership + inheritance !!!

# dir access  / or: to consider what ALREADY exists
setfacl -m    g:profs:rwx  /opt/research
setfacl -m    g:interns:rx /opt/research

# file access / or: to consider what will be NEW
setfacl -m  d:g:profs:rw   /opt/research
setfacl -m  d:g:interns:r  /opt/research
 
setfacl -m    o::-         /opt/research # ?
  

# admins ro
setfacl -m d:g:admins:r  /test
setfacl -m   g:admins:r  /test

# matt rw
setfacl -m   u:matt:rwx /test
setfacl -m   u:cindy:rx /test

